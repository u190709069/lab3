import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in); //Creates an object to read user input
        Random rand = new Random(); //Creates an object from Random class
        int number =rand.nextInt(100); //generates a number between 0 and 99
        
        int emir = 0;
        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.print("Can you guess it: ");


        while (true) {
            int guess = reader.nextInt(); //Read the user input

            if (guess == number ){

                System.out.println("Congratulations! You won after " + emir + " attempts!" );
                break;
            }
            else if (guess == -1){
                System.out.print("Sorry, the number was " + number );
                break;
            }

            if (guess != number && guess > number  ){

                System.out.println("sorry!" );
                System.out.println("Mine is less than your guess. " );

            }
            else if (guess != number && guess < number ){
                System.out.println("sorry!" );
                System.out.println("Mine is greater than your guess. " );

            }


            System.out.print("Type -1 to quit or guess another: ");
            emir++;
        }


        reader.close(); //Close the resource before exiting
    }


}








